package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.RenderTickEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.chat.Chat;
import de.paxii.clarinet.util.module.settings.ValueBase;
import de.paxii.clarinet.util.settings.type.ClientSettingBoolean;

import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.chunk.Chunk;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;

/**
 * Created by Lars on 07.02.2016.
 */
public class ModuleChunkBorders extends Module {
  private boolean spawnChunks;
  private double[][] spawnChunkCorners;
  private Chunk[] borderizedChunks;
  private int prevChunkX;
  private int prevChunkZ;
  private boolean updateChunks;

  private ValueBase renderHeight;
  private ValueBase radius;

  public ModuleChunkBorders() {
    super("ChunkBorders", ModuleCategory.RENDER);
    this.setCommand(true);
    this.setSyntax("chunkborders autoheight");
    this.setDescription("Renders the borders of Chunks");
    this.setVersion("1.0.1");
    this.setBuildVersion(16001);

    this.renderHeight = new ValueBase("ChunkBorders Height", 64.0F, 0.0F, 256F);
    this.radius = new ValueBase("Chunkborders Radius", 2.0F, 1.0F, 16.0F);
    this.getModuleValues().put("height", this.renderHeight);
    this.getModuleValues().put("radius", this.radius);
    this.getModuleSettings().put("auto", new ClientSettingBoolean("Auto Height", true));

    this.spawnChunks = false;
    this.spawnChunkCorners = new double[4][2];
    this.prevChunkX = 0;
    this.prevChunkZ = 0;
    this.updateChunks = false;
  }

  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this);

    this.updateSpawnChunks(Wrapper.getWorld());
  }

  @EventHandler
  public void onRender(RenderTickEvent event) {
    final EntityPlayer player = Wrapper.getPlayer();
    final WorldClient world = Wrapper.getWorld();
    final double renderY = this.getValueOrDefault("auto", Boolean.class, true) ? 0.1D : (renderHeight.getValue() - Wrapper.getMinecraft().getRenderManager().viewerPosY);

    if (this.prevChunkX != player.chunkCoordX || this.prevChunkZ != player.chunkCoordZ || this.borderizedChunks == null || this.updateChunks) {
      this.updateChunks = false;
      this.prevChunkX = player.chunkCoordX;
      this.prevChunkZ = player.chunkCoordZ;
      this.updateChunkList(world, Wrapper.getPlayer(), (int) this.radius.getValue());
    }

    glPushMatrix();
    glColor4f(1.0F, 0.0F, 0.3F, 1.0F);
    glDisable(GL_TEXTURE_2D);
    GlStateManager.disableTexture2D();
    final Tessellator tessellator = Tessellator.getInstance();
    final VertexBuffer worldRenderer = tessellator.getBuffer();
    for (final Chunk chunk : this.borderizedChunks) {
      if (chunk == null) {
        this.updateChunks = true;
      } else {
        double chunkX = ((double) chunk.xPosition * 16D) - Wrapper.getMinecraft().getRenderManager().viewerPosX;
        double chunkZ = ((double) chunk.zPosition * 16D) - Wrapper.getMinecraft().getRenderManager().viewerPosZ;

        worldRenderer.begin(2, DefaultVertexFormats.POSITION);
        worldRenderer.pos(chunkX, renderY, chunkZ).endVertex();
        worldRenderer.pos(chunkX + 16.0, renderY, chunkZ).endVertex();
        worldRenderer.pos(chunkX + 16.0, renderY, chunkZ + 16.0).endVertex();
        worldRenderer.pos(chunkX, renderY, chunkZ + 16.0).endVertex();
        tessellator.draw();
      }
    }
    if (this.spawnChunks && this.isPosInRenderableArea(player, world.getSpawnPoint().getX(), world.getSpawnPoint().getZ())) {
      this.renderSpawnArea(world, renderY);
      this.renderSpawnChunks(world, renderY);
    }
    glEnable(GL_TEXTURE_2D);
    glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
    glPopMatrix();
  }

  private void renderSpawnArea(WorldClient world, double renderY) {
    double spawnX = (world.getSpawnPoint().getX() - Wrapper.getMinecraft().getRenderManager().viewerPosX);
    double spawnZ = (world.getSpawnPoint().getZ() - Wrapper.getMinecraft().getRenderManager().viewerPosZ);
    Tessellator tessellator = Tessellator.getInstance();
    VertexBuffer worldRenderer = tessellator.getBuffer();
    glColor4f(0F, 1F, 0F, 1F);
    tessellator.getBuffer().begin(2, DefaultVertexFormats.POSITION);
    worldRenderer.pos((spawnX + 10D), renderY + 0.1D, (spawnZ + 10D)).endVertex();
    worldRenderer.pos((spawnX + 10D), renderY + 0.1D, (spawnZ - 10D)).endVertex();
    worldRenderer.pos((spawnX - 10D), renderY + 0.1D, (spawnZ - 10D)).endVertex();
    worldRenderer.pos((spawnX - 10D), renderY + 0.1D, (spawnZ + 10D)).endVertex();
    tessellator.draw();
  }

  private void renderSpawnChunks(WorldClient world, double renderY) {
    Tessellator tessellator = Tessellator.getInstance();
    VertexBuffer worldRenderer = tessellator.getBuffer();
    glColor4f(0F, 1F, 1F, 1F);
    worldRenderer.begin(2, DefaultVertexFormats.POSITION);
    worldRenderer.pos(this.spawnChunkCorners[0][0] - Wrapper.getMinecraft().getRenderManager().viewerPosX, renderY + 0.1D, this.spawnChunkCorners[0][1] - Wrapper.getMinecraft().getRenderManager().viewerPosZ).endVertex();
    worldRenderer.pos(this.spawnChunkCorners[1][0] - Wrapper.getMinecraft().getRenderManager().viewerPosX, renderY + 0.1D, this.spawnChunkCorners[1][1] - Wrapper.getMinecraft().getRenderManager().viewerPosZ).endVertex();
    worldRenderer.pos(this.spawnChunkCorners[2][0] - Wrapper.getMinecraft().getRenderManager().viewerPosX, renderY + 0.1D, this.spawnChunkCorners[2][1] - Wrapper.getMinecraft().getRenderManager().viewerPosZ).endVertex();
    worldRenderer.pos(this.spawnChunkCorners[3][0] - Wrapper.getMinecraft().getRenderManager().viewerPosX, renderY + 0.1D, this.spawnChunkCorners[3][1] - Wrapper.getMinecraft().getRenderManager().viewerPosZ).endVertex();
    tessellator.draw();
  }

  private void updateChunkList(WorldClient world, EntityPlayer player, int radius) {
    int startChunkX = player.chunkCoordX - radius;
    int startChunkZ = player.chunkCoordZ - radius;
    int sideLength = radius * 2 + 1;
    this.borderizedChunks = new Chunk[sideLength * sideLength];
    int currentIndex = 0;
    for (int row = 0; row < sideLength; ++row) {
      for (int column = 0; column < sideLength; ++column) {
        Chunk chunk = world.getChunkFromChunkCoords(startChunkX + row, startChunkZ + column);
        if (chunk.isLoaded()) {
          this.borderizedChunks[currentIndex] = chunk;
          ++currentIndex;
        }
      }
    }
  }

  private void updateSpawnChunks(WorldClient world) {
    boolean centerChunk = false;
    if (world.getSpawnPoint().getX() / 16.0 % 0.5 == 0.0 && world.getSpawnPoint().getZ() / 16.0 % 0.5 == 0.0) {
      centerChunk = true;
    }
    int midX = Math.round(world.getSpawnPoint().getX() / 16) * 16;
    int midZ = Math.round(world.getSpawnPoint().getZ() / 16) * 16;
    this.spawnChunkCorners[0][0] = midX + 96 + (centerChunk ? 16 : 0);
    this.spawnChunkCorners[0][1] = midZ + 96 + (centerChunk ? 16 : 0);
    this.spawnChunkCorners[1][0] = midX + 96 + (centerChunk ? 16 : 0);
    this.spawnChunkCorners[1][1] = midZ - 96;
    this.spawnChunkCorners[2][0] = midX - 96;
    this.spawnChunkCorners[2][1] = midZ - 96;
    this.spawnChunkCorners[3][0] = midX - 96;
    this.spawnChunkCorners[3][1] = midZ + 96 + (centerChunk ? 16 : 0);
  }

  private boolean isPosInRenderableArea(EntityPlayer player, int posX, int posZ) {
    return getDistanceSq(player.posX, player.posZ, posX, posZ) < 36864.0;
  }

  private double getDistanceSq(double x1, double z1, double x2, double z2) {
    double xs = x1 - x2;
    double zs = z1 - z2;
    return xs * xs + zs * zs;
  }

  @Override
  public void onCommand(String[] args) {
    if (args.length >= 1) {
      if (args[0].equalsIgnoreCase("autoheight")) {
        this.getModuleSettings().put("auto", new ClientSettingBoolean("Auto Height", !this.getValueOrDefault("auto", Boolean.class, false)));

        Chat.printClientMessage(String.format("ChunkBorders AutoHeight has been set to %b", this.getValue("auto", Boolean.class)));
      } else {
        Chat.printChatMessage("Unknown Subcommand!");
      }
    }
  }

  @Override
  public void onDisable() {
    Wrapper.getEventManager().unregister(this);
  }
}

